

module SSGaussQuad

using QuadGK
import QuadGK.gauss

# Library for indexing
include("./ssindexingtools/indexingtools.jl")
using .SSIndexingTools
import .SSIndexingTools.IndexForRowCol

"""
function NumGaussPts(p::Int64)
========

# Parameters
* p: Degree of the polynomial (or, polynomial order)

# Returns:
* The number of gauss points required
    to obtain the exact integral under
    the curve for this degree polynomial in 1D
"""
function NumGaussPts(p::Int64)
    numgausspts = convert(Int64, ceil((p+1)/2))
    return numgausspts
end



"""
struct GaussQuadTable
========

# Attributes:
* pts: The quadrature points, each point in a column,
          each row a dimension.
* wts: The quadrature weights
* count: The number of quadrature points and weights.

# Constructor GaussQuadTable(ngp)
* ngp: The number of gaussian quadrature points

# Constructor GaussQuadTable(ngp_xi,ngp_eta)
* ngp_xi: The number of gaussian quadrature points (xi-direction)
* ngp_eta: The number of gaussian quadrature points (eta-direction)
"""
mutable struct GaussQuadTable
    pts
    wts
    count

    function GaussQuadTable(ngp::Int64)
        self = new()
        m = convert(Int64,ngp)
        self.pts,self.wts = gauss(Float64,m)
        if(length(size(self.pts)) == 1)
            # Make sure each point is in its own column, even
            #  though there is only one value per column.
            self.pts = self.pts'
        end
        self.count = length(self.wts)
        return self
    end

    function GaussQuadTable(ngp_xi::Int64,ngp_eta::Int64)
        self = new()

        table1D_xi = GaussQuadTable(ngp_xi)
        table1D_eta = GaussQuadTable(ngp_eta)

        num_pts_xi = length(table1D_xi.pts)
        num_pts_eta = length(table1D_eta.pts)
        num_pts = num_pts_xi*num_pts_eta
        self.pts = zeros(2,num_pts)
        self.wts = zeros(num_pts)
        self.count = num_pts

        for j = 1:length(table1D_eta.pts)
            for i = 1:length(table1D_xi.pts)
                pt_i = IndexForRowCol(i,j,num_pts_xi)
                self.pts[1,pt_i] = table1D_xi.pts[i]
                self.pts[2,pt_i] = table1D_eta.pts[j]
                self.wts[pt_i] = table1D_xi.wts[i]*table1D_eta.wts[j]
            end
        end

        return self
    end
end


"""
function GaussQuadIntegrate(f::Function,
                            GQT::GaussQuadTable,
                            jacobian::Function)
========

 This function uses Gaussian Quadrature to evaluate an integral.
    It assumes the function to be integrated is a function of the
    parameter-space variables (xi in 1D, or xi,eta in 2D, etc.) but
    that the integration limits have been changed to be -1 to 1 in
    each direction, and that a jacobian is necessary to obtain the
    solution for the original real-space variables (x in 1D, or x,y
    in 2D).

# Parameters:
* f: Function of the parameter-space variables (xi in 1D, or xi,eta
    in 2D, etc.) that takes as input an array of Float64 values
    representing the parameter-space point to be evaluated, and
    returns the value of the function at that point.
* GQTable: The GaussQuadTable object that holds the gauss quadrature
    points and weights.
* jacobian: Function of the parameter-space variables (xi in 1D, or
    xi,eta in 2D, etc.) that takes as input an array of Float64
    values respresenting the parameter-space point to be evaluated,
    and returns the jacobian for transforming from the parameter-space
    integration limits to the
    real-space integration limits at that point.

# Returns
* The result is either one value or an array the size returned
    from the function f.
"""
function GaussQuadIntegrate(f::Function,
                            GQT::GaussQuadTable,
                            jacobian::Function)
    result = nothing
    # iterate over the gauss points, and sum each piece.
    for i = 1:GQT.count
        if(length(size(GQT.pts)) > 1)
            f_val = f(GQT.pts[:,i])
            jac_val = jacobian(GQT.pts[:,i])
        else
            f_val = f( [GQT.pts[i]] )
            jac_val = jacobian( [GQT.pts[i]] )
        end
        # The word "addend" is defined as: "a number which is added to another"
        addend = (f_val * GQT.wts[i] * jac_val)
        if( result == nothing )
            result = addend
        else
            result += addend
        end
    end
    return result
end





"""
function GaussQuadIntegrate(f::Array{Float64},
                            wts::Array{Float64},
                            jacobian::Array{Float64})
========

 This function uses Gaussian Quadrature to evaluate an integral.
   It assumes the domain goes from -1 to 1, but that there is a
   jacobian to transform the answer to what it would be for different
   integral bounds.

# Parameters:
* f: Array containing the evaluations of the function at each gauss point.
* wts: Array containing the weights for each gauss point.
* jacobian: Array containing the evaluations of the jacobian for transforming
    from the parameter-space integration limits (-1 to 1) to the
    domain-space integration limits, for each gauss point.

# Returns
* The result is either one value or an array the size returned
    from the function f.
"""
function GaussQuadIntegrate(f::Array{Float64},
                            wts::Array{Float64},
                            jacobian::Array{Float64})
    count = length(f)
    result = 0
    for i = 1:count
        result += f[i] * wts[i] * jacobian[i]
    end
    return result
end



end # module SSGaussQuad
